; joy-down-p.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Test UI Joystick Down, returning T if pressed, nil otherwise

; Note you need to run tft2:init at the start of your program
; to configure the input pins

(defun tft2:joy-down-p nil
  (not (seesaw:digital-read-p 'PA07))
  )

; end of joy-down-p.lisp file
