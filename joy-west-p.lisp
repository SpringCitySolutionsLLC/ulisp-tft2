; joy-west-p.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Test UI Joystick West, returning T if pressed, nil otherwise

; Note you need to run tft2:init at the start of your program
; to configure the input pins

(defun tft2:joy-west-p nil
  (not (seesaw:digital-read-p 'PA06))
  )

; end of joy-west-p.lisp file
