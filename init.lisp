; init.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Initialization routine
; Run once at the start of every program using a TFTv2 board.

(defun tft2:init nil

  ; Software reset the seesaw chip
  (seesaw:reset)

  ; ButtonA ButtonB ButtonC
  (seesaw:digital-direction 'PA10 'input-pull-up)
  (seesaw:digital-direction 'PA11 'input-pull-up)
  (seesaw:digital-direction 'PA14 'input-pull-up)

  ; JoyN JoyE JoyS JoyW JoyDown
  (seesaw:digital-direction 'PA05 'input-pull-up)
  (seesaw:digital-direction 'PA09 'input-pull-up)
  (seesaw:digital-direction 'PA08 'input-pull-up)
  (seesaw:digital-direction 'PA06 'input-pull-up)
  (seesaw:digital-direction 'PA07 'input-pull-up)

  (tft2:backlight 50)

  (display:init)

  (eval t))

; end of init.lisp file
