; button-c-p.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Test UI Button C, returning T if pressed, nil otherwise

; Note you need to run tft2:init at the start of your program
; to configure the input pins

(defun tft2:button-c-p nil
  (not (seesaw:digital-read-p 'PA14))
  )

; end of button-c-p.lisp file
