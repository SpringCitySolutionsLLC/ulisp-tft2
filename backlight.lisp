; backlight.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Control the backlight brighness with simple decimal percent

(defun tft2:backlight (percent)
  (setq percent (ceiling (* 2.55 percent)))
  (seesaw:pwm 'PA04 percent)
  )

; end of backlight.lisp file
