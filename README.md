# ulisp-tft2

A uLisp library for the Adafruit (tm) Arduino TFT shield V2 Part number 802.

https://www.adafruit.com/product/802

This wraps various functions from the seesaw library

https://gitlab.com/SpringCitySolutionsLLC/ulisp-seesaw

and the st7735R library

https://gitlab.com/SpringCitySolutionsLLC/ulisp-st7735r

to provide an easy to use and intuitive device specific API

The uLisp language home page is at http://www.ulisp.com

See the project wiki page at

https://gitlab.com/SpringCitySolutionsLLC/ulisp-tft2/wikis/home
