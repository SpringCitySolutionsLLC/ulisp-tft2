; joy-east-p.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Test UI Joystick East, returning T if pressed, nil otherwise

; Note you need to run tft2:init at the start of your program
; to configure the input pins

(defun tft2:joy-east-p nil
  (not (seesaw:digital-read-p 'PA09))
  )

; end of joy-east-p.lisp file
