; button-a-p.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Test UI Button A, returning T if pressed, nil otherwise

; Note you need to run tft2:init at the start of your program
; to configure the input pins

(defun tft2:button-a-p nil
  (not (seesaw:digital-read-p 'PA10))
  )

; end of button-a-p.lisp file
